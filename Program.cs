using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TryCatchEnum
{
    class Program
    {
        //CLASE ENUM
        enum Level
        {
            Low,
            Medium,
            High
        }

        //CLASE ENUM MES
        enum Mes
        {
            Enero,
            Febrero,
            Marzo,
            Abril,
            Mayo,
            Junio,
            Julio,
            Agosto,
            Septiembre,
            Octubre,
            Noviembre,
            Diciembre
        }

        static void checkAge(int age)
        {


            if (age < 18)
            {
                throw new ArithmeticException("Acceso denegado - Debes tener mínimo 18 años ");
            }
            else
            {
                Console.WriteLine("Acceso concedido - Tienes la edad suficiente");
                Console.Read();
            }
        }
        static void Main(string[] args)
        {
            //checkAge(21);

            //Try y Catch
            try
            {
                int[] myNumbers = { 1, 2, 3 };
                Console.WriteLine(myNumbers[10]);
            }
            catch (Exception e)
            {
                Console.WriteLine("Something went wrong.");
            }
            finally
            {
               Console.WriteLine("El 'try catch' finalizo");
                //CLASE ENUM
                //Level myVar = Level.Medium;

                //CLASE ENUM MES
                Mes myVar = Mes.Marzo;
                Console.WriteLine(myVar);
            }

            Console.Read();
        }
    }
}
